
import java.util.Scanner;

public class Game {

    Table table;

    Game() {
    }

    public void startGame() {
        showWelcome();
        Table table = new Table();
        Player player = new Player();
        run(table, player);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTurn(Player player) {
        System.out.println(player.getPlayer() + " Turn");
    }

    public boolean outAr(String row, String col) {
        if (Integer.parseInt(row) > 3 || Integer.parseInt(col) > 3) {
            System.out.println("Please enter only values number 1 to 3. ");
            return false;
        }
        return true;
    }

    public boolean isNot(String row, String col) {
       try{
           Integer.parseInt(row);
           Integer.parseInt(col);
       }catch(Exception e){
           System.out.println("Please enter only values number 1 to 3. ");
        return false;
       }return true;
    }

    public void checkInput(Player player,Table table) {
        String row, col;
        Scanner kb = new Scanner(System.in);
        while (true) {
            try{
            System.out.print("Please input row col: ");
            row = kb.next();
            col = kb.next();
            if (outAr(row, col) == false) {
                continue;          
            } else {
                break;
            }
            }catch(Exception e){
                System.out.println("Please enter only values number 1 to 3. ");
            }
        }
        input(player,row, col, table);
    }
    public static void input(Player player, String row, String col, Table table) {
        table.setTable(player, Integer.parseInt(row) - 1, Integer.parseInt(col) - 1);
    }

    public void showWin(Player player, boolean end,Table table) {
        if (end == true) {
             table.getTable();
            System.out.println(player.getPlayer() + " Win");
        }
    }

    public boolean run(Table table, Player player) {
        boolean end = false;
        while (!end) {
            table.getTable();
            showTurn(player);
            checkInput(player,table);
            end = table.checkWin(player);               
            showWin(player, end,table);
            player.addTurn(); 
        }
       
        return true;
    }
}
